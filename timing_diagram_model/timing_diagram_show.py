import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import csv




def shou_contrast_line():
    x1,y1,z1 = np.loadtxt("161005FGTH.csv",delimiter=',',unpack=True)
    x2,y2,z2 = np.loadtxt("000051HS300.csv",delimiter=',',unpack=True)

    plt.title("Yield Curve")
    plt.plot(x1,y1,label='161005:FGTH')
    plt.plot(x2,y2,label='000051:HS300')
    plt.xlabel('Time')
    plt.ylabel('Price')

    plt.legend()
    plt.show()

def shou_contrast_point():
    x1,y1,z1 = np.loadtxt("161005FGTH.csv",delimiter=',',unpack=True)
    x2,y2,z2 = np.loadtxt("000051HS300.csv",delimiter=',',unpack=True)

    plt.title("Yield Curve")
    plt.scatter(z1,z2)
    plt.xlabel('161005FGTH')
    plt.ylabel('000051HS300')

    plt.legend()
    plt.show()
    
shou_contrast_point()