#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Description:       :
@Date     :2022/03/31 15:18:42
@Author      :xia
@version      :1.0
'''

import math
import time 
import datetime
from typing import List
import json
import copy
import os
def calculate_profit_taking(position:{}, cur:{}, rate: float) -> List[float]:
    """
    @description  :         Determine whether your expected rate of return has been reached
    ---------
    @param  : position:{}   your position
    @param  : cur:{}        cur value
    @param  : rate: float   your expected rate of return
    -------
    @Returns  :
    1.negative              return the rate
    2.positive              but not meet expectations, return the rate
    3.positive              and meet expectations, return the rate and available time
    -------
    """
    present_value = 0
    future_value = 0
    dp = 0
    df = 0

    # traversal position dict to get present value
    for key,value in position.items():
        present_value = value
        dp = datetime.datetime.strptime(key,'%Y-%m-%dT%H:%M:%S')
    # traversal cur dict to get future value
    for key,value in cur.items():
        future_value = value
        df = datetime.datetime.strptime(key,'%Y-%m-%dT%H:%M:%S')
    
    # your profit 
    profit = future_value - present_value

    # dur time
    delta_dur_time = df - dp
    dur_day = delta_dur_time.days
    
    #real rate in simple interest
    real_rate_si = profit/present_value/dur_day

    #Compound Interest
    #see Compound Interest Calculation Formula in README.md
    real_rate_ci = (math.pow(future_value/present_value,1/dur_day)-1)

    #rate for day
    idea_rate = rate/365

    #append and return
    res = []
    #res.append(real_rate_si)
    res.append(real_rate_ci*365)
    if idea_rate < real_rate_ci:
        #see Holding Time Calculation Formula in README.md
        idea_rate /= 365
        real_rate_ci /= 365
        res_day = dur_day*(math.log(1+real_rate_ci)/math.log(1+idea_rate)-1)
        res.append(res_day)
    return res

def add_position(position:{}) -> int:
    """
    @description  :         add new position to file(position.json)
    ---------
    @param  : position:{}   your position
    -------
    @Returns  :
    0.success
    -------
    """
    tf = open("position.json", "r")
    if os.path.getsize("position.json"):
        position_old = json.load(tf)
    else:
        #first position
        tf.close()
        tf = open("position.json", "w")
        json.dump(position,tf)
        tf.close()
        return 0
    tf.close()

    for i in position:
        position_old[i] = position[i]

    tf = open("position.json", "w")
    json.dump(position_old,tf)
    tf.close()
    return 0

def load_and_calculate_position() -> {}:
    """
    @description  :        load position by file(position.json) and calculate avg position
    ---------
    @param  :
    -------
    @Returns  :            avg position
    -------
    """
    #load
    tf = open("position.json", "r")
    position = json.load(tf)
    position_copy = copy.deepcopy(position) 
    tf.close()
    #calculate avg_position.
    #see Average Time Cost Algorithm in README.md
    #1.find the start time
    start_time = get_min_time(position)
    del position[start_time.isoformat()]
    #2.traversal position to calculate avg position
    while position:
        new_time = get_min_time(position)
        del position[new_time.isoformat()]
        dur_day = new_time - start_time
        sum_value = position_copy[new_time.isoformat()] + position_copy[start_time.isoformat()]
        add_time = position_copy[new_time.isoformat()] / (position_copy[new_time.isoformat()] + position_copy[start_time.isoformat()]) * dur_day
        start_time = start_time + add_time
        position_copy[start_time.isoformat()] = sum_value
    avg_position = dict()
    avg_position[start_time.isoformat()] = position_copy[start_time.isoformat()]
    return avg_position

def get_min_time(position:{}) -> datetime:
    """
    @description  :         get min time in position
    ---------
    @param  :               position
    -------
    @Returns  :             min time
    -------
    """
    start_time = datetime.datetime.strptime('2030-04-22T00:00:00', '%Y-%m-%dT%H:%M:%S')
    for key in position:
        delta_dur_time = datetime.datetime.strptime(key,'%Y-%m-%dT%H:%M:%S') - start_time
        if delta_dur_time.days < 0:
             start_time = datetime.datetime.strptime(key,'%Y-%m-%dT%H:%M:%S')
    return start_time

input_position = dict()
cur_value = dict()
rate = 0.07

cur_time = time.strftime("%Y-%m-%d", time.localtime())
dp = datetime.datetime.strptime('2020-04-26', '%Y-%m-%d')
df = datetime.datetime.strptime(cur_time,'%Y-%m-%d')

'''
#input_position 
#The present value here refers to the total cost,present value = price * quantity
input_position[dp.isoformat()] = 10
add_position(input_position)
'''

#load_and_calculate_position
avg_position = dict()
avg_position = load_and_calculate_position()
cur_value[df.isoformat()] = 35

res = calculate_profit_taking(avg_position,cur_value,rate)
print(res)
